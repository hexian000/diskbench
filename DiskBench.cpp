﻿// DiskBench.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <WinSDKVer.h>
#include <SDKDDKVer.h>

#include <stdio.h>
#include <tchar.h>

#include <cstdint>
#include <chrono>
#include <random>
#include <functional>
#include <locale>
#include <iostream>

#define NOMINMAX
#include <Windows.h>

using namespace std;

constexpr decltype(chrono::steady_clock::now)* now = chrono::high_resolution_clock::is_steady ? &chrono::high_resolution_clock::now : &chrono::steady_clock::now;

void init() {
	setlocale(LC_ALL, "");
	_tprintf_s(_T("Disk Bench 2.0\n"));
	_tprintf_s(_T("  By: He Xian Copyright(c) 2019\n"));
	_tprintf_s(_T("\n"));
	_tprintf_s(_T("* 1 GB = 1024 MB, 1 MB = 1024 KB, 1 KB = 1024 B\n"));
	_tprintf_s(_T("\n"));
}

void printError(const TCHAR* format, DWORD errId) {
	_tprintf_s(_T("%s\n"), format);

	LPTSTR errorText = NULL;

	DWORD dwSuccess = FormatMessage(
		// use system message tables to retrieve error text
		FORMAT_MESSAGE_FROM_SYSTEM
		// allocate buffer on local heap for error text
		| FORMAT_MESSAGE_ALLOCATE_BUFFER
		// Important! will fail otherwise, since we're not 
		// (and CANNOT) pass insertion parameters
		| FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,    // unused with FORMAT_MESSAGE_FROM_SYSTEM
		errId,
		GetUserDefaultUILanguage(),
		(LPTSTR)& errorText,  // output 
		0, // minimum size for output buffer
		NULL);   // arguments - see note 

	if (dwSuccess && NULL != errorText) {
		_tprintf_s(_T("错误%u：%s\n"), errId, errorText);
		LocalFree(errorText);
		errorText = NULL;
	}
	else _tprintf_s(_T("错误%u：（无法获取错误信息）\n"), errId);
}

LPVOID AlignedAlloc(const size_t size, const size_t alignment, LPVOID& lpReserve) {
	lpReserve = VirtualAlloc(NULL, size + alignment, MEM_RESERVE, PAGE_NOACCESS);
	if (lpReserve == NULL) {
		printError(_T("MEM_RESERVE Error"), GetLastError());
		return NULL;
	}
	LPVOID lpAlloc = lpReserve;
	if ((reinterpret_cast<size_t>(lpAlloc) & (alignment - 1)) != 0) {
		lpAlloc = reinterpret_cast<LPVOID>((reinterpret_cast<size_t>(lpAlloc) + alignment) & ~(alignment - 1));
	}
	LPVOID lpCommit = VirtualAlloc(lpAlloc, size, MEM_COMMIT, PAGE_READWRITE);
	if (lpReserve == NULL) {
		printError(_T("MEM_COMMIT Error"), GetLastError());
		return NULL;
	}
	return lpCommit;
}

int64_t runSequential(const HANDLE hDisk, const uint64_t size, const uint64_t start, const uint64_t span, const size_t alignment) {
	{
		uint64_t newPointer;
		BOOL bOK = SetFilePointerEx(hDisk, *reinterpret_cast<const LARGE_INTEGER*>(&start),
			reinterpret_cast<LARGE_INTEGER*>(&newPointer), FILE_BEGIN);
		if (bOK != TRUE) {
			printError(_T("SetFilePointerEx Error"), GetLastError());
			return -1;
		}
		if (start != newPointer) {
			_tprintf_s(_T("SetFilePointerEx mismatch: want=0x%llX actual=0x%llX"), start, newPointer);
			return -1;
		}
	}

	const auto bufSize = min(span, 1ull << 22);
	LPVOID lpReserve;
	LPVOID lpBuffer = AlignedAlloc(bufSize, alignment, lpReserve);
	if (lpBuffer == NULL) {
		return -1;
	}
	DWORD dwTryRead = static_cast<DWORD>(bufSize), dwRead;

	using namespace std::chrono;
	auto clock_start = now();
	for (auto remain = span; remain > 0; remain -= static_cast<uint64_t>(dwRead)) {
		if (static_cast<uint64_t>(dwTryRead) > remain) dwTryRead = static_cast<DWORD>(remain);
		BOOL bOK = ReadFile(hDisk, lpBuffer, dwTryRead, &dwRead, NULL);
		if (bOK != TRUE) {
			printError(_T("ReadFile Error"), GetLastError());
			if (VirtualFree(lpReserve, 0, MEM_RELEASE) == FALSE) {
				printError(_T("VirtualFree Error"), GetLastError());
			}
			return -1;
		}
		if (dwTryRead != dwRead) {
			_tprintf_s(_T("ReadFile not finished: want=0x%X actual=0x%X"), dwTryRead, dwRead);
			if (VirtualFree(lpReserve, 0, MEM_RELEASE) == FALSE) {
				printError(_T("VirtualFree Error"), GetLastError());
			}
			return -1;
		}
	}
	auto clock_end = now();

	if (VirtualFree(lpReserve, 0, MEM_RELEASE) == FALSE) {
		printError(_T("VirtualFree Error"), GetLastError());
		return -1;
	}
	return (duration_cast<nanoseconds>(clock_end - clock_start)).count();
}

int64_t runRandom(const HANDLE hDisk, const uint64_t size, const uint64_t start, const uint64_t range, const uint64_t block, const size_t alignment, const int sample) {
	LPVOID lpReserve;
	LPVOID lpBuffer = AlignedAlloc(block, alignment, lpReserve);
	if (lpBuffer == NULL) {
		return -1;
	}
	DWORD dwTryRead = (DWORD)block, dwRead;
	BOOL bOK;

	const uint64_t slot = range / block;
	mt19937_64 e{ 0x12345678 };
	uniform_int_distribution<uint64_t> distribution(0ull, slot - 1ull);

	using namespace std::chrono;
	auto clock_start = now();
	for (int i = 0; i < sample; i++) {
		uint64_t pos = start + (block * distribution(e));

		uint64_t newPointer;
		bOK = SetFilePointerEx(hDisk, *reinterpret_cast<LARGE_INTEGER*>(&pos),
			reinterpret_cast<LARGE_INTEGER*>(&newPointer), FILE_BEGIN);
		if (bOK != TRUE) {
			printError(_T("SetFilePointerEx Error"), GetLastError());
			if (VirtualFree(lpReserve, 0, MEM_RELEASE) == FALSE) {
				printError(_T("VirtualFree Error"), GetLastError());
			}
			return -1;
		}
		if (pos != newPointer) {
			_tprintf_s(_T("SetFilePointerEx mismatch: want=0x%llX actual=0x%llX"), pos, newPointer);
			if (VirtualFree(lpReserve, 0, MEM_RELEASE) == FALSE) {
				printError(_T("VirtualFree Error"), GetLastError());
			}
			return -1;
		}

		bOK = ReadFile(hDisk, lpBuffer, dwTryRead, &dwRead, NULL);
		if (bOK != TRUE) {
			printError(_T("ReadFile Error"), GetLastError());
			if (VirtualFree(lpReserve, 0, MEM_RELEASE) == FALSE) {
				printError(_T("VirtualFree Error"), GetLastError());
			}
			return -1;
		}
		if (dwTryRead != dwRead) {
			_tprintf_s(_T("ReadFile not finished: want=0x%X actual=0x%X"), dwTryRead, dwRead);
			if (VirtualFree(lpReserve, 0, MEM_RELEASE) == FALSE) {
				printError(_T("VirtualFree Error"), GetLastError());
			}
			return -1;
		}
	}
	auto clock_end = now();
	if (VirtualFree(lpReserve, 0, MEM_RELEASE) == FALSE) {
		printError(_T("VirtualFree Error"), GetLastError());
		return -1;
	}
	return (duration_cast<nanoseconds>(clock_end - clock_start)).count();
}

int64_t runSkip(const HANDLE hDisk, const uint64_t size, const uint64_t span, const uint64_t block, const size_t alignment, const int sample) {
	LPVOID lpReserve;
	LPVOID lpBuffer = AlignedAlloc(block, alignment, lpReserve);
	if (lpBuffer == NULL) {
		return -1;
	}
	DWORD dwTryRead = (DWORD)block, dwRead;
	BOOL bOK;

	bool forward = true;
	uint64_t start = 0;

	const uint64_t slot = span / block;
	mt19937_64 e{ 0x12345678 };
	uniform_int_distribution<uint64_t> distribution(0ull, slot - 1ull);

	using namespace std::chrono;
	auto clock_start = now();
	for (int i = 0; i < sample; i++) {
		uint64_t pos = start + (block * distribution(e));
		uint64_t newPointer;
		bOK = SetFilePointerEx(hDisk, *reinterpret_cast<LARGE_INTEGER*>(&pos),
			reinterpret_cast<LARGE_INTEGER*>(&newPointer), FILE_BEGIN);
		if (bOK != TRUE) {
			printError(_T("SetFilePointerEx Error"), GetLastError());
			if (VirtualFree(lpReserve, 0, MEM_RELEASE) == FALSE) {
				printError(_T("VirtualFree Error"), GetLastError());
			}
			return -1;
		}
		if (pos != newPointer) {
			_tprintf_s(_T("SetFilePointerEx mismatch: want=0x%llX actual=0x%llX"), pos, newPointer);
			if (VirtualFree(lpReserve, 0, MEM_RELEASE) == FALSE) {
				printError(_T("VirtualFree Error"), GetLastError());
			}
			return -1;
		}

		bOK = ReadFile(hDisk, lpBuffer, dwTryRead, &dwRead, NULL);
		if (bOK != TRUE) {
			printError(_T("ReadFile Error"), GetLastError());
			if (VirtualFree(lpReserve, 0, MEM_RELEASE) == FALSE) {
				printError(_T("VirtualFree Error"), GetLastError());
			}
			return -1;
		}
		if (dwTryRead != dwRead) {
			_tprintf_s(_T("ReadFile not finished: want=0x%X actual=0x%X"), dwTryRead, dwRead);
			if (VirtualFree(lpReserve, 0, MEM_RELEASE) == FALSE) {
				printError(_T("VirtualFree Error"), GetLastError());
			}
			return -1;
		}

		if (!forward && (start < span)) start = span, forward = true;
		else if (forward && (start + (2 * span) >= size)) start -= span, forward = false;
		else if (forward) start += span;
		else start -= span;
	}
	auto clock_end = now();
	if (VirtualFree(lpReserve, 0, MEM_RELEASE) == FALSE) {
		printError(_T("VirtualFree Error"), GetLastError());
		return -1;
	}
	return (duration_cast<nanoseconds>(clock_end - clock_start)).count();
}

int64_t filter_min(const function<int64_t()>& f) {
	int64_t min = -1;
	for (auto i = 0; i < 5; i++) {
		Sleep(100);
		auto r = f();
		if (r < 0) return r;
		if (r < min || min < 0) min = r;
	}
	return min;
}

#define ALIGN(offset,align_to) (((offset) / (align_to)) * (align_to))

bool runSequentialSuite(const HANDLE hDisk, const uint64_t size, const uint64_t block, const size_t alignment) {
	int64_t time;
	const double span_mb = static_cast<double>(block) / 1048576.0;

	time = filter_min([=]()-> int64_t {
		return runSequential(hDisk, size, 0, block, alignment);
	});
	if (time < 0) return false;
	_tprintf_s(_T("  head:   %.2lfMB/s\n"), span_mb / ((double)time / 1e+9));
	time = filter_min([=]()-> int64_t {
		return runSequential(hDisk, size, ALIGN((size - block) >> 1, alignment), block, alignment);
	});
	if (time < 0) return false;
	_tprintf_s(_T("  middle: %.2lfMB/s\n"), span_mb / ((double)time / 1e+9));
	time = filter_min([=]()-> int64_t {
		return runSequential(hDisk, size, ALIGN(size - block, alignment), block, alignment);
	});
	if (time < 0) return false;
	_tprintf_s(_T("  tail:   %.2lfMB/s\n"), span_mb / ((double)time / 1e+9));
	return true;
}

bool runRandomSuite(const HANDLE hDisk, const uint64_t size, const uint64_t range, const uint64_t block, const size_t alignment) {
	int64_t time;
	constexpr int sample = 1000;
	constexpr double sample_d = static_cast<double>(sample);
	if (range < size) {
		time = filter_min([=]()-> int64_t {
			return runRandom(hDisk, size, 0, range, block, alignment, sample);
		});
		if (time < 0) return false;
		_tprintf_s(_T("  head:   %.2lf IOPS\n"), sample_d / ((double)time / 1e+9));
		time = filter_min([=]()-> int64_t {
			return runRandom(hDisk, size, ALIGN((size - range) >> 1, alignment), range, block, alignment, sample);
		});
		if (time < 0) return false;
		_tprintf_s(_T("  middle: %.2lf IOPS\n"), sample_d / ((double)time / 1e+9));
		time = filter_min([=]()-> int64_t {
			return runRandom(hDisk, size, ALIGN(size - range, alignment), range, block, alignment, sample);
		});
		if (time < 0) return false;
		_tprintf_s(_T("  tail:   %.2lf IOPS\n"), sample_d / ((double)time / 1e+9));
	}
	else { // range == size
		time = filter_min([=]()-> int64_t {
			return runRandom(hDisk, size, 0, size, block, alignment, sample);
		});
		if (time < 0) return false;
		_tprintf_s(_T("  full:   %.2lf IOPS\n"), sample_d / ((double)time / 1e+9));
	}
	return true;
}

bool runSkipSuite(const HANDLE hDisk, const uint64_t size, const uint64_t block, const uint64_t span, const size_t alignment) {
	int64_t time;
	constexpr int sample = 1000;
	constexpr double sample_d = static_cast<double>(sample);
	time = filter_min([=]()-> int64_t {
		return runSkip(hDisk, size, span, block, alignment, sample);
	});
	if (time < 0) return false;
	_tprintf_s(_T("  full:   %.2lf IOPS\n"), sample_d / ((double)time / 1e+9));
	return true;
}

int run(DWORD drive) {
	TCHAR strPath[MAX_PATH];
	_stprintf_s(strPath, MAX_PATH, _T("\\\\.\\PhysicalDrive%u"), drive);
	_tprintf_s(_T("Path: %s\n"), strPath);
	HANDLE hDisk = CreateFile(strPath, GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING,
		FILE_FLAG_NO_BUFFERING, NULL);
	if (hDisk == INVALID_HANDLE_VALUE) {
		printError(L"CreateFile Error", GetLastError());
		return EXIT_FAILURE;
	}
	BOOL bOK;
	uint64_t size, sector;
	{
		GET_LENGTH_INFORMATION DiskSize;
		DWORD dwReturned;
		bOK = DeviceIoControl(hDisk, IOCTL_DISK_GET_LENGTH_INFO, NULL, 0, (LPVOID)& DiskSize, sizeof(GET_LENGTH_INFORMATION), &dwReturned, NULL);
		if (bOK != TRUE) {
			printError(_T("IOCTL_DISK_GET_LENGTH_INFO Error"), GetLastError());
			bOK = CloseHandle(hDisk);
			if (bOK != TRUE) {
				printError(_T("CloseHandle Error"), GetLastError());
			}
			return EXIT_FAILURE;
		}
		if (dwReturned != sizeof(GET_LENGTH_INFORMATION)) {
			_tprintf_s(_T("WTF: IOCTL_DISK_GET_LENGTH_INFO returned unexpected length\n"));
			bOK = CloseHandle(hDisk);
			if (bOK != TRUE) {
				printError(_T("CloseHandle Error"), GetLastError());
			}
			return EXIT_FAILURE;
		}
		size = DiskSize.Length.QuadPart;
		_tprintf_s(_T("Disk size: %llu GB\n"), size >> 30);
	}
	{
		STORAGE_PROPERTY_QUERY query;
		query.PropertyId = STORAGE_PROPERTY_ID::StorageAccessAlignmentProperty;
		query.QueryType = STORAGE_QUERY_TYPE::PropertyStandardQuery;
		query.AdditionalParameters[0] = 0;
		STORAGE_ACCESS_ALIGNMENT_DESCRIPTOR desc;
		DWORD dwReturned;
		bOK = DeviceIoControl(hDisk, IOCTL_STORAGE_QUERY_PROPERTY, (LPVOID)& query, sizeof(STORAGE_PROPERTY_QUERY),
			(LPVOID)& desc, sizeof(STORAGE_ACCESS_ALIGNMENT_DESCRIPTOR), &dwReturned, NULL);
		if (bOK != TRUE) {
			printError(_T("IOCTL_STORAGE_QUERY_PROPERTY Error"), GetLastError());
			bOK = CloseHandle(hDisk);
			if (bOK != TRUE) {
				printError(_T("CloseHandle Error"), GetLastError());
			}
			return EXIT_FAILURE;
		}
		if (dwReturned != sizeof(STORAGE_ACCESS_ALIGNMENT_DESCRIPTOR)) {
			_tprintf_s(_T("WTF: IOCTL_STORAGE_QUERY_PROPERTY returned unexpected length\n"));
			bOK = CloseHandle(hDisk);
			if (bOK != TRUE) {
				printError(_T("CloseHandle Error"), GetLastError());
			}
			return EXIT_FAILURE;
		}
		sector = desc.BytesPerPhysicalSector;
	}
	if (sector < 4096ull) sector = 4096ull;
	_tprintf_s(_T("Benchmark area aligned to: %llu B\n"), sector);

	_tprintf_s(_T("\n"));
	_tprintf_s(_T("Sequential read 1GB:\n"));
	if (!runSequentialSuite(hDisk, size, 1ull << 30, sector)) {
		bOK = CloseHandle(hDisk);
		if (bOK != TRUE) {
			printError(_T("CloseHandle Error"), GetLastError());
		}
		return EXIT_FAILURE;
	}

	_tprintf_s(_T("\n"));
	_tprintf_s(_T("Random read 4K block in 8G span:\n"));
	if (!runRandomSuite(hDisk, size, 1ull << 33, 4096ull, sector)) {
		bOK = CloseHandle(hDisk);
		if (bOK != TRUE) {
			printError(_T("CloseHandle Error"), GetLastError());
		}
		return EXIT_FAILURE;
	}

	_tprintf_s(_T("\n"));
	_tprintf_s(_T("Random read 4K block in 16G span:\n"));
	if (!runRandomSuite(hDisk, size, 1ull << 34, 4096ull, sector)) {
		bOK = CloseHandle(hDisk);
		if (bOK != TRUE) {
			printError(_T("CloseHandle Error"), GetLastError());
		}
		return EXIT_FAILURE;
	}

	_tprintf_s(_T("\n"));
	_tprintf_s(_T("Random read 4K block in 8G skip:\n"));
	if (!runSkipSuite(hDisk, size, 4096ull, 1ull << 33, sector)) {
		bOK = CloseHandle(hDisk);
		if (bOK != TRUE) {
			printError(_T("CloseHandle Error"), GetLastError());
		}
		return EXIT_FAILURE;
	}

	bOK = CloseHandle(hDisk);
	if (bOK != TRUE) {
		printError(_T("CloseHandle Error"), GetLastError());
		return EXIT_FAILURE;
	}

	_tprintf_s(_T("\n\n"));
	return EXIT_SUCCESS;
}

int _tmain() {
	init();
	DWORD drive = 0;
	_tprintf_s(_T("Physical drive(e.g. 0): "));
	_tscanf_s(_T("%u"), &drive);
	int ret = run(drive);
	system("pause");
	return ret;
}
